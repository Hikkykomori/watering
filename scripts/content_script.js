const s = document.createElement('script')
s.src = chrome.runtime.getURL('scripts/injected.js')
var pageTitleNotification = {
    Vars:{
        OriginalTitle: document.title,
        Interval: null
    },    
    on: function(notification, intervalSpeed){
        var _this = this;
        _this.Vars.Interval = setInterval(function(){
             document.title = (_this.Vars.OriginalTitle == document.title)
                                 ? notification
                                 : _this.Vars.OriginalTitle;
        }, (intervalSpeed) ? intervalSpeed : 1000);
    },
    off: function(){
        clearInterval(this.Vars.Interval);
        document.title = this.Vars.OriginalTitle;   
    }
}
window.addEventListener("message", (event) => {
    console.log("event received")
    if (event.source != window) {
        console.log("wrong source")
        return;
    }
    if (event.data.type && (event.data.type == "ALERT")) {
        console.log(event)
        document.querySelector("link[rel*='icon']").href = "https://poisondrop.ru/images/favicons/favicon-32.png";
        pageTitleNotification.on("СЮДА")
        let sound = chrome.runtime.getURL("sound/sound.mp3")
        new Audio(sound).play()
    }
})
s.onload = async function () {
    this.remove()
};
(document.head || document.documentElement).appendChild(s)